# xfs_tools
*purpose: To rapidly move data Across backups & primary stor!*

xfs_copy, xfsdump, xfsrestore
- doc: https://linux.die.net/man/8/xfs_copy
- https://linux.die.net/man/8/xfsdump
- https://linux.die.net/man/8/xfsrestore
- desc: xfs_copy copies an XFS filesystem to one or more targets in parallel. 